from abc import ABCMeta, abstractmethod


class Agent:
    """
    An abstract agent.
    """

    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def name(self):
        """
        Gets the agent's name.

        :return: The agent's name.
        """
        pass

    @abstractmethod
    def act(self, state):
        """
        Makes the agent choose a legal action for the given state.

        :param state: The state on which to act.

        :return: The chosen legal action to do in the given state.
        """
        pass
