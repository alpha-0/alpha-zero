import os

import arrow
from keras.models import clone_model, load_model

from alpha_zero import AlphaZero
from examples.connect_four.connect_four_environment import \
    ConnectFourEnvironment
from examples.connect_four.connect_four_game_status import ConnectFourGameStatus
from examples.connect_four.connect_four_machine_agent import \
    ConnectFourMachineAgent
from examples.connect_four.connect_four_model import ConnectFourModel
from memory import Memory


def play_game(agent1, agent2, memory:Memory=None):
    env = ConnectFourEnvironment()
    state = env.gen_init_state()

    while True:
        status = env.check_game_status(state)
        if status == ConnectFourGameStatus.P1WIN:
            print('Player 1 wins!')
            break
        elif status == ConnectFourGameStatus.P2WIN:
            print('Player 2 wins!')
            break
        elif status == ConnectFourGameStatus.DRAW:
            print('It is a draw!')
            break

        agent = agent1 if state.initiative == 1 else agent2
        action = agent.act(state)

        if memory is not None:
            memory.add_to_st_mem(state, agent.policy)

        state = env.apply_action(state, action)

    if memory is not None:
        memory.update_st_mem(state.value)
        memory.transfer_st_mem_to_lt_mem()

    return status


def compute_score(results1, results2, num_games):
    score1 = sum(1 for r in results1 if r == ConnectFourGameStatus.P1WIN)
    score2 = sum(1 for r in results2 if r == ConnectFourGameStatus.P2WIN)
    return (score1 + score2) / num_games


if __name__ == '__main__':
    BEST_MODEL_PATH = os.path.join('.', 'models', 'best_model')
    EXPLORATION_RATE = 0.2
    NUM_SIMULATIONS = 1000
    MEMORY_LEN = 2000

    NUM_SELF_PLAY_GAMES = 10

    NUM_TRAINING_STEPS = 10
    TRAINING_SAMPLE_SIZE = 100
    TRAINING_BATCH_SIZE = 100
    TRAINING_EPOCHS = 20

    NUM_SELF_PLAY_TRAINING = 5

    NUM_CHALLENGE_GAMES = 10
    CHALLENGE_DETHRONING_SCORE = 0.6

    os.makedirs(os.path.dirname(BEST_MODEL_PATH), exist_ok=True)

    memory = Memory()

    best_model = ConnectFourModel(load_model(BEST_MODEL_PATH)
                                  if os.path.exists(BEST_MODEL_PATH) else None)
    best_agent = ConnectFourMachineAgent(best_model, NUM_SIMULATIONS, 0.0)
    model = ConnectFourModel(load_model(BEST_MODEL_PATH)
                             if os.path.exists(BEST_MODEL_PATH) else None)
    agent1 = ConnectFourMachineAgent(model, NUM_SIMULATIONS, EXPLORATION_RATE)
    agent2 = ConnectFourMachineAgent(model, NUM_SIMULATIONS, EXPLORATION_RATE)

    while True:
        for _ in range(NUM_SELF_PLAY_TRAINING):
            print('SELF PLAY')
            AlphaZero.self_play(
                play_game, agent1, agent2, NUM_SELF_PLAY_GAMES, memory)

            print('MODEL TRAINING')
            AlphaZero.train(
                model.build_model_input, model, memory, MEMORY_LEN,
                TRAINING_SAMPLE_SIZE, TRAINING_BATCH_SIZE, TRAINING_EPOCHS,
                NUM_TRAINING_STEPS)

        print('CHALLENGE')
        agent1.exploration_rate = 0.0
        score, winner, is_best_dethroned = AlphaZero.challenge(
            play_game, agent1, best_agent, NUM_CHALLENGE_GAMES, compute_score,
            CHALLENGE_DETHRONING_SCORE)
        print(f'Challenger score: {score}')
        agent1.exploration_rate = EXPLORATION_RATE
        if is_best_dethroned:
            t = arrow.utcnow().format('YYYYMMDDHHmmss')
            model.model.save(f'{BEST_MODEL_PATH}_{t}.h5')
            best_model.model = clone_model(model.model)
            best_model.model.set_weights(model.model.get_weights())
            print('Best model has been dethroned!')
