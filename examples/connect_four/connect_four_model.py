import keras
import numpy as np
from keras import Input
from keras.layers import Activation, BatchNormalization, Conv2D, Dense, \
    Flatten, add
from tensorflow import name_scope

from examples.connect_four.connect_four_environment import \
    ConnectFourEnvironment
from model import Model


class ConnectFourModel(Model):
    """
    A Connect Four model.
    """

    def __init__(self, model=None):
        """
        Initializes a ConnectFourModel.

        :param model: The model's model. If None, it gets created.
        """
        self._env = ConnectFourEnvironment()
        if model is None:
            self._model = self._build_model(
                (*self._env.GRID_SHAPE, 15), self._env.max_num_actions)
        else:
            self._model = model

    @property
    def model(self):
        """
        Gets the model's model.

        :return: The model's model.
        """
        return self._model

    @model.setter
    def model(self, value):
        """
        Sets the model's value.

        :param value: The value to set.
        """
        self._model = value

    def predict(self, state):
        """
        Predicts the given state's value and prior action probabilities.

        :param state: The state to predict on.

        :return: The predicted state value and prior action probabilities.
        """
        v, pi = self._model.predict(self.build_model_input(state))
        return v[0][0], pi[0]

    def build_model_input(self, state):
        """
        Builds a model input from a given state.

        :param state: The state to convert into a model input.

        :return: The build model input from the given state.
        """
        states = state.history
        history_len = 7
        last_states = list(reversed(states[-min(history_len, len(states)):]))
        p1_pieces_history = []
        p2_pieces_history = []
        for state in last_states:
            p1_pieces = np.zeros(self._env.GRID_SHAPE)
            p2_pieces = np.zeros(self._env.GRID_SHAPE)
            p1_pieces[state.board == 1] = 1
            p2_pieces[state.board == 2] = 1
            p1_pieces_history.append(p1_pieces)
            p2_pieces_history.append(p2_pieces)
        if len(last_states) < history_len:
            n = history_len - len(last_states)
            p1_pieces_history.extend([np.zeros(self._env.GRID_SHAPE)] * n)
            p2_pieces_history.extend([np.zeros(self._env.GRID_SHAPE)] * n)
        initiative = np.ones(self._env.GRID_SHAPE) \
            if states[-1].initiative == 1 else np.zeros(self._env.GRID_SHAPE)
        return np.stack((np.stack(
            (*p1_pieces_history, *p2_pieces_history, initiative), axis=-1),))

    @staticmethod
    def _build_model(input_shape, policy_len):
        """
        Builds the model.

        :param input_shape: The input's shape.
        :param policy_len: The length of the policy.

        :return: The built model.
        """
        with name_scope('input'):
            state_input = Input(shape=input_shape)

        with name_scope('initial_convolution'):
            h = ConnectFourModel.add_conv_block(state_input, 16, (3, 3), 1)

        with name_scope('residual_complex'):
            for n in range(3):
                h = ConnectFourModel.add_residual_block(h, 16, (3, 3), n)

        with name_scope('value_complex'):
            v = ConnectFourModel.add_conv_block(h, 1, (1, 1), 'v_1')
            v = Flatten()(v)
            v = Dense(64, activation='relu', name='value_dense_1')(v)
            value_output = Dense(1, activation='tanh', name='value')(v)

        with name_scope('policy_complex'):
            p = ConnectFourModel.add_conv_block(h, 2, (1, 1), 'p_1')
            p = Flatten()(p)
            policy_output = Dense(
                policy_len, activation='softmax', name='policy')(p)

        model = keras.Model(
            inputs=[state_input],
            outputs=[value_output, policy_output])
        model.compile(
            optimizer='adam',
            loss={'value': 'mse', 'policy': 'mse'},
            loss_weights={'value': 0.5, 'policy': 0.5})
        return model

    @staticmethod
    def add_conv_block(x, filters, kernel_size, n):
        """
        Adds a convolutional block.

        :param x: The block's input.
        :param filters: The number of convolutional filters.
        :param kernel_size: The convolution kernel size.
        :param n: The name of the block.

        :return: The built convolutional block.
        """
        with name_scope(f'conv_block_{n}'):
            r = Conv2D(filters, kernel_size, padding='same',
                       name=f'conv_block_{n}_conv')(x)
            r = BatchNormalization(name=f'conv_block_{n}_batch_norm')(r)
            r = Activation('relu', name=f'conv_block_{n}_relu')(r)
            return r

    @staticmethod
    def add_residual_block(x, filters, kernel_size, n):
        """
        Adds a residual block.

        :param x: The block's input.
        :param filters: The number of convolutional filters.
        :param kernel_size: The convolution kernel size.
        :param n: The name of the block.

        :return: The built residual block.
        """
        with name_scope(f'residual_block_{n}'):
            r = Conv2D(filters, kernel_size, padding='same',
                       name=f'residual_block_{n}_conv_a')(x)
            r = BatchNormalization(name=f'residual_block_{n}_batch_norm_a')(r)
            r = Activation('relu', name=f'residual_block_{n}_relu_a')(r)
            r = Conv2D(filters, kernel_size, padding='same',
                       name=f'residual_block_{n}_conv_b')(r)
            r = BatchNormalization(name=f'residual_block_{n}_batch_norm_b')(r)
            r = add([x, r], name=f'residual_block_{n}_skip_connection')
            r = Activation('relu', name=f'residual_block_{n}_relu_b')(r)
            return r
