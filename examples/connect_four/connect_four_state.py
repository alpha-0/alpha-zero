import numpy as np

from state import State


class ConnectFourState(State):
    """
    A Connect Four state.
    """

    def __init__(self, board: np.ndarray, initiative,
                 value: float, is_terminal: bool, history=None):
        """
        Initializes a ConnectFourState.

        :param board: The state's game board.
        :param initiative: The state's initiative.
        :param value: The state's value.
        :param is_terminal: If the state is terminal.
        :param history: The state's history.
        """
        self._board = board
        self._id = ''.join(map(str, board.reshape(-1).tolist()))
        self._initiative = initiative
        self._value = value
        self._is_terminal = is_terminal
        self._history = (history if history is not None else []) + [self]

    @property
    def id(self):
        """
        Gets the state's identifier.

        :return: The state's identifier.
        """
        return self._id

    @property
    def value(self):
        """
        Gets the state's value.

        :return: The state's value.
        """
        return self._value

    @property
    def is_terminal(self):
        """
        Determines if this state is terminal.

        :return: If this state is terminal.
        """
        return self._is_terminal

    @property
    def initiative(self):
        """
        Gets the state's initiative.

        :return: The state's initiative.
        """
        return self._initiative

    @property
    def history(self):
        """
        Gets the state's history.

        :return: The state's history.
        """
        return self._history

    @property
    def board(self):
        """
        Gets the state's game board.

        :return: The state's game board.
        """
        return self._board
