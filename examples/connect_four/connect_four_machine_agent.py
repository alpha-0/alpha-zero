import numpy as np

from agent import Agent
from examples.connect_four.connect_four_environment import \
    ConnectFourEnvironment
from mcts import MonteCarloTreeSearch, MonteCarloTreeSearchNode
from memory import Memory
from model import Model


class ConnectFourMachineAgent(Agent):
    """
    A Connect Four machine agent.
    """

    def __init__(self, model: Model, num_simulations: int,
                 exploration_rate: float):
        """
        Initializes a ConnectFourMachineAgent.

        :param model: The predictive model tp use for Monte Carlo Tree Search.
        :param num_simulations: Number of Monte Carlo simulation per turn.
        :param exploration_rate: The agent's initial exploration rate.
        """
        self._num_simulations = num_simulations
        self._exploration_rate = exploration_rate
        self._environment = ConnectFourEnvironment()
        self._model = model
        self._mcts = MonteCarloTreeSearch(
            root=MonteCarloTreeSearchNode(self._environment.gen_init_state()),
            environment=self._environment, model=self._model)
        self._memory = Memory()

    @property
    def name(self):
        """
        Gets the agent's name.

        :return: The agent's name.
        """
        return 'AlphaConnectFour Zero'

    @property
    def policy(self):
        """
        Gets the agent's policy.

        :return: The agent's policy.
        """
        edges = self._mcts.root.child_edges
        actions = [edge.action for edge in edges]
        ns = [edge.n for edge in edges]
        policy = np.zeros((self._environment.max_num_actions,))
        policy[actions] = ns / np.sum(ns)
        return policy

    @property
    def mcts(self):
        """
        Gets the agent's Monte Carlo Tree Search.

        :return: tTe agent's Monte Carlo Tree Search.
        """
        return self._mcts

    @property
    def exploration_rate(self):
        """
        Gets the agent's exploration rate.

        :return: The agent's exploration rate.
        """
        return self._exploration_rate

    @exploration_rate.setter
    def exploration_rate(self, value: float):
        """
        Sets the agent's exploration rate.

        :param value: The value to set.
        """
        self._exploration_rate = value

    @property
    def model(self):
        """
        Gets the agent's model.

        :return: The agent's model.
        """
        return self._model

    def act(self, state):
        """
        Makes the agent choose a legal action for the given state.

        :param state: The state on which to act.

        :return: The chosen legal action to do in the given state.
        """
        self._mcts.update_root(MonteCarloTreeSearchNode(state))

        for _ in range(self._num_simulations):
            self._mcts.run_simulation_round()

        return self._select_action()

    def _select_action(self):
        """
        Selects an action based on policy and exploration rate.

        :return: The selected action.
        """
        edges = self._mcts.root.child_edges
        ns = [edge.n for edge in edges]
        if np.random.rand() < self._exploration_rate:
            # We choose an action stochastically (from N's distribution)
            actions = [edge.action for edge in edges]
            return np.random.choice(actions, p=ns / np.sum(ns))
        else:
            # We chose an action deterministically (the one with max N)
            return edges[np.argmax(ns)].action
