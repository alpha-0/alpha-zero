from agent import Agent
from examples.connect_four.connect_four_environment import \
    ConnectFourEnvironment


class ConnectFourConsoleUserAgent(Agent):
    """
    A Connect Four console user agent.
    """

    def __init__(self, name):
        """
        Initializes a ConnectFourConsoleUserAgent.

        :param name: The user agent's name.
        """
        self._name = name
        self._env = ConnectFourEnvironment()

    @property
    def name(self):
        """
        Gets the agent's name.

        :return: The agent's name.
        """
        return self._name

    def act(self, state):
        """
        Makes the agent choose a legal action for the given state.

        :param state: The state on which to act.

        :return: The chosen legal action to do in the given state.
        """
        legal_actions = [a + 1 for a in self._env.legal_actions(state)]
        user_input = None
        while user_input not in legal_actions:
            try:
                user_input = int(input('Choose an action: '))
                break
            except:
                continue
        return user_input - 1
