import numpy as np

from environment import Environment
from examples.connect_four.connect_four_state import ConnectFourState
from examples.connect_four.connect_four_game_status import ConnectFourGameStatus


class ConnectFourEnvironment(Environment):
    """
    A Connect Four environment.
    """

    GRID_SHAPE = (6, 7)
    DTYPE = np.byte
    IN_PROGRESS_VALUE = 0.0
    WIN_VALUE = 1.0
    DRAW_VALUE = 0.0
    LOSS_VALUE = -1.0
    TERMINAL_P1_VALUES = {
        ConnectFourGameStatus.P1WIN: WIN_VALUE,
        ConnectFourGameStatus.DRAW: DRAW_VALUE,
        ConnectFourGameStatus.P2WIN: LOSS_VALUE
    }
    ALIGNMENTS = np.array([
        [0, 1, 2, 3],
        [1, 2, 3, 4],
        [2, 3, 4, 5],
        [3, 4, 5, 6],
        [7, 8, 9, 10],
        [8, 9, 10, 11],
        [9, 10, 11, 12],
        [10, 11, 12, 13],
        [14, 15, 16, 17],
        [15, 16, 17, 18],
        [16, 17, 18, 19],
        [17, 18, 19, 20],
        [21, 22, 23, 24],
        [22, 23, 24, 25],
        [23, 24, 25, 26],
        [24, 25, 26, 27],
        [28, 29, 30, 31],
        [29, 30, 31, 32],
        [30, 31, 32, 33],
        [31, 32, 33, 34],
        [35, 36, 37, 38],
        [36, 37, 38, 39],
        [37, 38, 39, 40],
        [38, 39, 40, 41],

        [0, 7, 14, 21],
        [7, 14, 21, 28],
        [14, 21, 28, 35],
        [1, 8, 15, 22],
        [8, 15, 22, 29],
        [15, 22, 29, 36],
        [2, 9, 16, 23],
        [9, 16, 23, 30],
        [16, 23, 30, 37],
        [3, 10, 17, 24],
        [10, 17, 24, 31],
        [17, 24, 31, 38],
        [4, 11, 18, 25],
        [11, 18, 25, 32],
        [18, 25, 32, 39],
        [5, 12, 19, 26],
        [12, 19, 26, 33],
        [19, 26, 33, 40],
        [6, 13, 20, 27],
        [13, 20, 27, 34],
        [20, 27, 34, 41],

        [3, 9, 15, 21],
        [4, 10, 16, 22],
        [10, 16, 22, 28],
        [5, 11, 17, 23],
        [11, 17, 23, 29],
        [17, 23, 29, 35],
        [6, 12, 18, 24],
        [12, 18, 24, 30],
        [18, 24, 30, 36],
        [13, 19, 25, 31],
        [19, 25, 31, 37],
        [20, 26, 32, 38],

        [3, 11, 19, 27],
        [2, 10, 18, 26],
        [10, 18, 26, 34],
        [1, 9, 17, 25],
        [9, 17, 25, 33],
        [17, 25, 33, 41],
        [0, 8, 16, 24],
        [8, 16, 24, 32],
        [16, 24, 32, 40],
        [7, 15, 23, 31],
        [15, 23, 31, 39],
        [14, 22, 30, 38]
    ])

    @property
    def name(self):
        """
        Gets the environment's name.

        :return: The environment's name.
        """
        return 'Connect Four'

    @property
    def max_num_actions(self):
        """
        Gets the environment's maximal number of actions.

        :return: The environment's maximal number of actions.
        """
        return 7

    def legal_actions(self, state):
        """
        Determines the legal actions for the given state.

        :param state: The state for which to get legal actions.

        :return: The legal actions for the given state.
        """
        return np.flatnonzero(state.board[-1, :] == 0).tolist()

    def apply_action(self, state, action):
        """
        Applies the given action to the given state, returning the new state.

        :param state: A state.
        :param action: A legal action to apply to the given state.

        :return: The new state resulting from the application of the given
            action to the given state.
        """
        next_board = np.copy(state.board)
        for row in range(self.GRID_SHAPE[0]):
            if next_board[row, action] == self.DTYPE(0):
                initiative = state.initiative
                next_board[row, action] = initiative
                next_initiative = self._toggle_initiative(initiative)
                next_status = self._check_game_status(next_board)
                next_is_terminal = next_status != ConnectFourGameStatus\
                    .IN_PROGRESS
                next_value = self.IN_PROGRESS_VALUE
                if next_is_terminal:
                    next_value = self.TERMINAL_P1_VALUES[next_status]
                    if next_initiative == 2:
                        next_value *= -1
                next_state = ConnectFourState(next_board, next_initiative,
                                              next_value, next_is_terminal,
                                              state.history)
                return next_state

    def gen_init_state(self):
        """
        Generates the initial state for the Connect Four game.

        :return: the initial state for the Connect Four game.
        """
        return ConnectFourState(np.zeros(self.GRID_SHAPE, dtype=self.DTYPE),
                                1, 0.0, False)

    def _toggle_initiative(self, initiative):
        """
        Toggles the initiative from a player to the other.

        :param initiative: The previous initiative.

        :return: The next initiative.
        """
        return 2 if initiative == 1 else 1

    def check_game_status(self, state):
        """
        Checks the game status for the given state.

        :param state: The state to check.

        :return: The game status for the given state to check.
        """
        return self._check_game_status(state.board)

    def _check_game_status(self, board):
        """
        Checks the game status for the given game board.

        :param board: The game board to check.

        :return: The game status for the given game board to check.
        """
        align_vals = np.take(board.reshape(-1), self.ALIGNMENTS)
        p1_alignments = np.atleast_2d(np.take(self.ALIGNMENTS, np.squeeze(
            np.argwhere(np.all(align_vals == 1, axis=-1))), axis=0))
        if len(p1_alignments) > 0:
            return ConnectFourGameStatus.P1WIN
        p2_alignments = np.atleast_2d(np.take(self.ALIGNMENTS, np.squeeze(
            np.argwhere(np.all(align_vals == 2, axis=-1))), axis=0))
        if len(p2_alignments) > 0:
            return ConnectFourGameStatus.P2WIN
        if not np.any(board == self.DTYPE(0)):
            return ConnectFourGameStatus.DRAW
        return ConnectFourGameStatus.IN_PROGRESS
