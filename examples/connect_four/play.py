import os

import numpy as np
from keras.models import load_model

from examples.connect_four.connect_four_console_user_agent import \
    ConnectFourConsoleUserAgent
from examples.connect_four.connect_four_environment import \
    ConnectFourEnvironment
from examples.connect_four.connect_four_game_status import ConnectFourGameStatus
from examples.connect_four.connect_four_machine_agent import \
    ConnectFourMachineAgent
from examples.connect_four.connect_four_model import ConnectFourModel


def play_game(agent1, agent2):
    env = ConnectFourEnvironment()
    state = env.gen_init_state()

    while True:
        print(np.flipud(state.board))

        status = env.check_game_status(state)
        if status == ConnectFourGameStatus.P1WIN:
            print('Player 1 wins!')
            break
        elif status == ConnectFourGameStatus.P2WIN:
            print('Player 2 wins!')
            break
        elif status == ConnectFourGameStatus.DRAW:
            print('It is a draw!')
            break

        agent = agent1 if state.initiative == 1 else agent2
        action = agent.act(state)

        state = env.apply_action(state, action)
        print(f'Move: {action + 1}')

    return status


if __name__ == '__main__':
    BEST_MODEL_PATH = os.path.join('.', 'models', 'best_model.h5')
    NUM_SIMULATIONS = 1000

    best_model = ConnectFourModel(load_model(BEST_MODEL_PATH)
                                  if os.path.exists(BEST_MODEL_PATH) else None)
    best_agent = ConnectFourMachineAgent(best_model, NUM_SIMULATIONS, 0.0)

    user_agent = ConnectFourConsoleUserAgent('User')

    play_game(best_agent, user_agent)
