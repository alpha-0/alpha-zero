from enum import Enum


class ConnectFourGameStatus(Enum):
    """
    A Connect Four game status.
    """

    IN_PROGRESS = 0
    P1WIN = 1
    P2WIN = 2
    DRAW = 3
