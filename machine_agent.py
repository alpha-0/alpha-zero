from abc import ABCMeta, abstractmethod

from agent import Agent


class MachineAgent(Agent):
    """
    An abstract machine agent.
    """

    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def name(self):
        """
        Gets the agent's name.

        :return: The agent's name.
        """
        pass

    @property
    @abstractmethod
    def policy(self):
        """
        Gets the agent's policy.

        :return: The agent's policy.
        """
        pass

    @property
    @abstractmethod
    def mcts(self):
        """
        Gets the agent's Monte Carlo Tree Search.

        :return: tTe agent's Monte Carlo Tree Search.
        """
        pass

    @abstractmethod
    def act(self, state):
        """
        Makes the agent choose a legal action for the given state.

        :param state: The state on which to act.

        :return: The chosen legal action to do in the given state.
        """
        pass
