
class MemoryElement:
    """
    A memory element.
    """

    def __init__(self, state, policy, value=None):
        """
        Initializes a MemoryElement.

        :param state: The state.
        :param policy: The computed policy for the given state.
        :param value: The end result value of the given state.
        """
        self._state = state
        self._policy = policy
        self._value = value

    @property
    def state(self):
        """
        Gets the memory element's state.
        :return: The memory element's state.
        """
        return self._state

    @property
    def policy(self):
        """
        Gets the memory element's policy.

        :return: The memory element's policy.
        """
        return self._policy

    @property
    def value(self):
        """
        Gets the memory element's value.

        :return: The memory element's value.
        """
        return self._value

    @value.setter
    def value(self, value):
        """
        Sets the memory element's value.

        :param value: The value to set.
        """
        self._value = value


class Memory:
    """
    A memory.
    """

    def __init__(self):
        """
        Initializes a memory.
        """
        self._lt_mem = []
        self._st_mem = []

    @property
    def lt_mem(self):
        """
        Gets the memory's long-term memory.

        :return: The memory's long-term memory.
        """
        return self._lt_mem

    def add_to_st_mem(self, state, policy):
        """
        Adds a memory element to the short term memory.

        :param state: The state.
        :param policy: The computed policy for the given state.
        """
        self._st_mem.append(MemoryElement(state, policy))

    def update_st_mem(self, value):
        """
        Updates the short-term memory with an end result value.

        :param value: The end result value to update with.
        """
        for e in reversed(self._st_mem):
            value *= -1
            e.value = value

    def transfer_st_mem_to_lt_mem(self):
        """
        Transfers short-term memory to long-term memory.
        """
        self._lt_mem.extend(self._st_mem)
        self._st_mem = []
