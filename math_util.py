import numpy as np


def softmax(x, axis=-1):
    """
    Computes the softmax of x.

    :param x: The input array.
    :param axis: The axis along which the softmax is performed.

    :return: The softmax of x along the given axis.
    """
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis)
