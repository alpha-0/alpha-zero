import random

import numpy as np


class AlphaZero:
    """
    A utility providing the Alpha Zero high-level process steps.
    """

    @staticmethod
    def self_play(play_game, agent1, agent2, num_games, memory):
        """
        Runs a round of self play.

        :param play_game: A function which plays a game.
        :param agent1: First player agent.
        :param agent2: Second player agent.
        :param num_games: Number of games to play.
        :param memory: A memory.
        """
        for _ in range(num_games):
            play_game(agent1, agent2, memory)

    @staticmethod
    def train(build_model_input, model, memory, memory_len, sample_size,
              batch_size, num_epochs_per_round, num_steps):
        """
        Trains a predictive model on experiences contained in a memory.

        :param build_model_input: Function to build model inputs from states.
        :param model: Model to train.
        :param memory: Memory from which to get experiences.
        :param memory_len: How far in the past should examples be considered.
        :param sample_size: Number of experiences per sample.
        :param batch_size: Training batch size.
        :param num_epochs_per_round: Number of epochs per training round.
        :param num_steps: Number of training loops.
        """
        mem = memory.lt_mem[min(-memory_len, len(memory.lt_mem)):]
        for _ in range(num_steps):
            batch = random.sample(mem, min(sample_size, len(mem)))

            states = np.concatenate(
                tuple(build_model_input(b.state) for b in batch))

            policies = np.stack(tuple(b.policy for b in batch))
            values = np.stack(tuple(b.value for b in batch))

            model.model.fit(x=[states],
                            y=[values, policies],
                            batch_size=batch_size,
                            epochs=num_epochs_per_round,
                            verbose=1)

    @staticmethod
    def challenge(play_game, challenger, best, num_games,
                  compute_score, dethroning_score):
        """
        Runs a challenge in between a new challenger and the best player.

        :param play_game: A function which plays a game.
        :param challenger: The challenger player agent.
        :param best: The best player agent.
        :param num_games: The number of games to play.
        :param compute_score: A function which computes the challenger's score.
        :param dethroning_score: If reached, the challenger becomes the best.

        :return: The score, the winner, if the best was dethroned.
        """
        n = int(round(num_games / 2))
        results1 = [play_game(challenger, best) for _ in range(n)]
        results2 = [play_game(best, challenger) for _ in range(num_games - n)]
        score = compute_score(results1, results2, num_games)
        if score >= dethroning_score:
            return score, challenger, True
        else:
            return score, best, False
