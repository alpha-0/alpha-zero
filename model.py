from abc import ABCMeta, abstractmethod


class Model:
    """
    An abstract model.
    """

    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def model(self):
        """
        Gets the model's model.

        :return: The model's model.
        """
        pass

    @abstractmethod
    def predict(self, state):
        """
        Predicts the given state's value and prior action probabilities.

        :param state: The state to predict on.

        :return: The predicted state value and prior action probabilities.
        """
        pass
