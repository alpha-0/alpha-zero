import numpy as np

from environment import Environment
from state import State
from math_util import softmax


class MonteCarloTreeSearchNode:
    """
    A Monte Carlo Tree Search Node.
    """

    def __init__(self, state: State):
        """
        Initializes a MonteCarloTreeSearchNode.

        :param state: The state corresponding to this node.
        """
        self._id = state.id
        self._state = state
        self._child_edges = []

    @property
    def id(self):
        """
        Gets the node's identifier.

        :return: The node's identifier.
        """
        return self._id

    @property
    def state(self):
        """
        Gets the node's state.

        :return: The node's state.
        """
        return self._state

    @property
    def is_state_terminal(self):
        """
        Determines if this node's state is terminal.

        :return: If this node's state is terminal.
        """
        return self._state.is_terminal

    @property
    def state_value(self):
        """
        Gets the node's state's value.

        :return: The node's state's value.
        """
        return self._state.value

    @property
    def state_initiative(self):
        """
        Gets the node's state's initiative.

        :return: The node's state's initiative.
        """
        return self._state.initiative

    @property
    def child_edges(self):
        """
        Gets the node's child (outgoing) edges.
        :return:
        """
        return self._child_edges

    def add_child_edge(self, edge):
        """
        Adds a child (outgoing) edge to this node.

        :param edge: The edge to add to this node.
        """
        self._child_edges.append(edge)

    def is_leaf(self):
        """
        Determines if this node is a leaf node.

        :return: If this node is a leaf node.
        """
        return len(self._child_edges) <= 0


class MonteCarloTreeSearchEdge:
    """
    A Monte Carlo Tree Search Edge.
    """

    def __init__(self, in_node: MonteCarloTreeSearchNode, action,
                 prior_probability: float, out_node: MonteCarloTreeSearchNode):
        """
        Initializes a MonteCarloTreeSearchEdge.

        :param in_node: The input node.
        :param action: The action leading from input to output nodes.
        :param prior_probability: The prior probability of taking the action.
        :param out_node: The output node.
        """
        self._in_node = in_node
        self._action = action
        self._out_node = out_node

        # The number of times action 'a' has been taken from state 's'
        self._n = 0
        # The total value of the next states
        self._w = 0
        # The mean value of the next states
        self._q = 0
        # The prior probability of selecting action 'a'
        self._p = prior_probability

    @property
    def in_node(self):
        """
        Gets the edge's input node.

        :return: The edge's input node.
        """
        return self._in_node

    @property
    def action(self):
        """
        Gets the edge's action.

        :return: The edge's action.
        """
        return self._action

    @property
    def out_node(self):
        """
        Gets the edge's output node.

        :return: The edge's output node.
        """
        return self._out_node

    @property
    def n(self):
        """
        Gets the edge's N metric.
        (N: The number of times action 'a' has been taken from state 's'.)

        :return: The edge's N metric.
        """
        return self._n

    @property
    def w(self):
        """
        Gets the edge's W metric.
        (W: The total value of the next states.)

        :return: The edges's W metric.
        """
        return self._w

    @property
    def q(self):
        """
        Gets the edge's Q metric.
        (Q: The mean value of the next states.)
        :return: The edges's Q metric.
        """
        return self._q

    @property
    def p(self):
        """
        Gets the edge's P metric.
        (P: The prior probability of selecting action 'a')
        :return: The edges's P metric.
        """
        return self._p

    def update(self, v):
        """
        Updates this edge with the given value.

        :param v: The value to update the edge with.
        """
        self._n += 1
        self._w += v
        self._q = self._w / self._n


class MonteCarloTreeSearch:
    """
    A Monte Carlo Tree Search.
    """

    def __init__(self, root: MonteCarloTreeSearchNode, environment: Environment,
                 model, epsilon=0.01, alpha=0.5, gamma=0.25, kappa=30.0):
        """
        Initializes a MonteCarloTreeSearch.

        :param root: The root node of the tree.
        :param environment: The simulation environment.
        :param model: The evaluation predictive model.
        :param epsilon: Root node exploration factor.
        :param alpha: Root node exploration Dirichlet distribution alpha.
        :param gamma: U N-part influence factor.
        :param kappa: U N-part acceptable coverage factor.
        """
        self._root = root
        self._environment = environment
        self._model = model
        self._epsilon = epsilon
        self._alpha = alpha
        self._gamma = gamma
        self._kappa = kappa
        self._tree = {}
        self._add_node(root)

    @property
    def root(self):
        """
        Gets the algorithm's tree's root.

        :return: The algorithm's tree's root.
        """
        return self._root

    @property
    def tree(self):
        """
        Gets the algorithm's tree.

        :return: The algorithm's tree.
        """
        return self._tree

    def update_root(self, node):
        """
        Updates the root of the tree to the given node.

        :param node: The root node to update to.
        """
        if node in self._tree:
            self._root = self._tree[node.id]
            self._tree = {}
            nodes = [node]
            while len(nodes) > 0:
                n = nodes.pop()
                self._add_node(n)
                for edge in n.child_edges:
                    nodes.append(edge.out_node)
        else:
            self._tree = {}
            self._root = node
            self._add_node(self._root)

    def run_simulation_round(self):
        """
        Runs a round of Monte Carlo tree search.
        """

        # We perform the selection step to get to a leaf node
        leaf_node, root_to_leaf_path = self._select_leaf_node()

        # We evaluate the value of the leaf node and expand it if possible
        leaf_node_value = self._evaluate(leaf_node)

        # We backpropagate the leaf node's value through the taken tree path
        self._backpropagate(
            leaf_node.state_initiative, leaf_node_value, root_to_leaf_path)

    def _select_leaf_node(self):
        """
        Step 1: Selection
        Start from the root node R and select successive child nodes down to a
        leaf node L by taking the actions which maximise the Q + U metric.

        :return: The selected leaf node.
        """

        # Stores the path we are following down the tree
        path = []

        # Stores the current node
        current_node = self._root

        # While no leaf node is reached
        while not current_node.is_leaf():

            # We get and compute the Q, P and N values of child nodes
            qs = np.array([c.q for c in current_node.child_edges])
            ps = np.array([c.p for c in current_node.child_edges])
            if current_node == self._root:
                ps = (1 - self._epsilon) * ps + self._epsilon * \
                     np.random.dirichlet(
                         [self._alpha] * len(current_node.child_edges))
            ns = np.array([c.n for c in current_node.child_edges])

            # We compute the Q + U metric
            us = (1. - self._gamma) * ps + self._gamma * self._kappa / (1. + ns)
            qu = qs + us

            # We find the edge for which the Q + U metric is maximal
            max_qu_edge = current_node.child_edges[int(np.argmax(qu))]

            # We follow the Q + U maximal edge and add it to our path
            path.append(max_qu_edge)

            # We update the current edge to the one we just selected
            current_node = max_qu_edge.out_node

        # We return the current node for its expansion and simulation
        # We return the path for the backpropagation updates
        return current_node, path

    def _evaluate(self, leaf_node):
        """
        Step 2: Evaluation
        If L is terminal, return its value. If L is not terminal send the leaf
        node's state to the predictive model for it to estimate the state's
        value and and its actions's prior probabilities ans return those.

        :param leaf_node: The leaf node to evaluate.

        :return: The leaf node's (actual or estimated) value.
        """

        # If the node is terminal, we use its value and cannot expand it
        if leaf_node.is_state_terminal:
            mtcs_node_value = leaf_node.state_value
        # If the node is not terminal, we estimate its value and expand it
        else:
            # We estimate the new MTCS node's value and its child edges' prior
            # probabilities by using the supplied predictive model
            mtcs_node_value, action_probs = self._model.predict(leaf_node.state)

            # We remove illegal actions and normalize legal action
            # probabilities so that they sum up to one
            legal_actions = np.array(
                self._environment.legal_actions(leaf_node.state))
            prior_probs = softmax(action_probs[legal_actions])

            for legal_action, prior_prob in zip(legal_actions, prior_probs):
                # We apply the current action to get the next state
                next_state = self._environment.apply_action(
                    leaf_node.state, legal_action)

                # If the next state is already in the tree
                if next_state.id in self._tree:
                    # We reuse its existing node
                    node = self._tree[next_state.id]
                else:
                    # We create a new node with the next state
                    node = MonteCarloTreeSearchNode(next_state)
                    # We add the new node to the tree
                    self._add_node(node)

                # We create a new edge from the leaf node's state to the next
                # state through the taken legal action with the initial prior
                # probability as given by the predictive model
                edge = MonteCarloTreeSearchEdge(
                    leaf_node, legal_action, prior_prob, node)

                # We add the new edge to the leaf node's child edges
                leaf_node.add_child_edge(edge)

        return mtcs_node_value

    @staticmethod
    def _backpropagate(initiative, value, path):
        """
        Step 3: Backpropagation
        Use the new information supplied by the predictive model to update
        information in the edges on the path from the root R to the leaf L.

        :param initiative: Initiative of the last state of the game.
        :param value: Value of the last state of the game.
        :param path: Path taken along the tree.
        """

        # For each node along the path from the the root to the leaf
        for edge in path:

            # We update the node with the new value (minding point of view)
            is_self_point_of_view = edge.in_node.state_initiative == initiative
            edge.update((1 if is_self_point_of_view else -1) * value)

    def _add_node(self, node: MonteCarloTreeSearchNode):
        """
        Adds a node to the tree.

        :param node: The node to add to the tree.
        """
        self._tree[node.id] = node
