from abc import ABCMeta, abstractmethod


class Environment:
    """
    An abstract environment.
    """

    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def name(self):
        """
        Gets the environment's name.

        :return: The environment's name.
        """
        pass

    @property
    @abstractmethod
    def max_num_actions(self):
        """
        Gets the environment's maximal number of actions.

        :return: The environment's maximal number of actions.
        """
        pass

    @abstractmethod
    def legal_actions(self, state):
        """
        Determines the legal actions for the given state.

        :param state: The state for which to get legal actions.

        :return: The legal actions for the given state.
        """
        pass

    @abstractmethod
    def apply_action(self, state, action):
        """
        Applies the given action to the given state, returning the new state.

        :param state: A state.
        :param action: A legal action to apply to the given state.

        :return: The new state resulting from the application of the given
            action to the given state.
        """
        pass
