from abc import ABCMeta, abstractmethod


class State:
    """
    An abstract state.
    """

    __metaclass__ = ABCMeta

    @property
    @abstractmethod
    def id(self):
        """
        Gets the state's identifier.

        :return: The state's identifier.
        """
        pass

    @property
    @abstractmethod
    def value(self):
        """
        Gets the state's value.

        :return: The state's value.
        """
        pass

    @property
    @abstractmethod
    def is_terminal(self):
        """
        Determines if this state is terminal.

        :return: If this state is terminal.
        """
        pass

    @property
    @abstractmethod
    def initiative(self):
        """
        Gets the state's initiative.

        :return: The state's initiative.
        """
        pass

    @property
    @abstractmethod
    def history(self):
        """
        Gets the state's history.

        :return: The state's history.
        """
        pass
